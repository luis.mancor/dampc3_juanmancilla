
import 'dart:convert';
import 'package:flutter/services.dart';

class _MenuProvider{
  List<dynamic> opciones = [];
  
  Future<List<dynamic>> cargarData() async {
    final resp = await rootBundle.loadString('data/menu_opts.json');

    Map dataMap = json.decode(resp);
    //print(dataMap);

    opciones = dataMap['rutas'];
    //print(opciones);
    return opciones;
  }
}

final menuprovider = new _MenuProvider();
