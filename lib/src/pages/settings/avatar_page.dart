import 'package:flutter/material.dart';

class AvatarPage extends StatelessWidget {

  final options = ['Uno','Dos','Tres','Cuatro','Cinco'];
  //TextStyle styleText = TextStyle(color:Colors.red, fontSize: 25.0);

  List<Widget> _crearItemsCorta1(){
    return options.map((item){
      
      return Column(
        children: <Widget>[
          ListTile(title: Text(item), subtitle: Text('Cualquier cosa')),
          Divider()
        ],
      );

    }).toList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Avatar page'),
      ),
      /*
      body: ListView(
        children: _crearItemsCorta1()
      ),
      */
      
      body: Center(
        
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          
          
          children: <Widget>[

            Padding(
              padding: EdgeInsets.all(30.0),
              child:  Image(image: NetworkImage('https://mensajesdios.com/wp-content/uploads/2017/02/african-sunset-tanzania-africa.jpg'),),
            ),
            
            
            Padding(
              padding: EdgeInsets.all(30.0),
              child:               
              Image(image: NetworkImage('https://resizer.codigounico.com/resizer/resizer.php?imagen=https://www.codigounico.com/wp-content/uploads/sites/2/2019/02/y-las-25-mejores-playas-del-mundo-son-6.jpg&nuevoancho=750&crop=0'),),
     
            ),
       
          ],
        ),
      ),

      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.keyboard_backspace),
          onPressed: () => {
            Navigator.pop(context),
          },
      ),

    );
  }
}
