import 'package:flutter/material.dart';

class CardsPage extends StatelessWidget {

  final options = ['Uno','Dos','Tres','Cuatro','Cinco'];
  //TextStyle styleText = TextStyle(color:Colors.red, fontSize: 25.0);

  List<Widget> _crearItemsCorta1(){
    return options.map((item){
      
      return Column(
        children: <Widget>[
          ListTile(title: Text(item), subtitle: Text('Cualquier cosa')),
          Divider()
        ],
      );

    }).toList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Cards page'),
      ),

      body: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(Icons.adb, color: Colors.red, size: 50.0),
            Icon(Icons.beach_access, color: Colors.blue, size:36.0),
            Slider(
              min: 0,
              max: 100,
              value: 0,//_value,
              onChanged: (value) {
                //setState(() { _value = value;});
              },
            ),
          ],
        ),
      ),
      
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.keyboard_backspace),
          onPressed: () => {
            Navigator.pop(context),
          },
      ),
      /*
      body: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            FlutterLogo(
              size: 60.0,
            ),
            FlutterLogo(
              size: 60.0,
            ),
            FlutterLogo(
              size: 60.0,
            ),
          ],

        ),
      ),
      */

      /*
      body: Align(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('Uno', style: styleText),
            Text('Dos', style: styleText),
            Text('Tres', style: styleText),
          ],
        ),
      ),
      */

    );
  }
}
