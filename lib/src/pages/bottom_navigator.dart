
import 'package:flutter/material.dart';
import 'package:tarea/src/pages/contacts_page.dart';
import 'package:tarea/src/pages/settings.dart';
import 'package:tarea/src/pages/homepage.dart';

class MyBottomNavigation extends StatefulWidget {
  @override
  _MyBottomNavigationState createState() => _MyBottomNavigationState();
}

class _MyBottomNavigationState extends State<MyBottomNavigation> {
  int _selectedIndex  = 0;

  final List<Widget> _children =[
    HomePage(),
    ContactsPage(),
    Settings(),
    
  ];

  @override
  Widget build(BuildContext context) {
     return Scaffold(
      body: _children[_selectedIndex],
  
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('Home'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.contacts),
            title: Text('Contactos'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            title: Text('Configuracion'),
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.amber[800],
        onTap: _onItemTapped,
      ),
    );
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

}
