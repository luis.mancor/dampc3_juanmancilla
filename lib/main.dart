import 'package:flutter/material.dart';
import 'package:tarea/src/pages/bottom_navigator.dart';
//import 'package:tarea/src/pages/homepage.dart';
import 'package:tarea/src/pages/settings/alert_page.dart';
import 'package:tarea/src/pages/settings/avatar_page.dart';
import 'package:tarea/src/pages/settings/cards_page.dart';
import 'package:tarea/src/pages/settings/animated_page.dart';
import 'package:tarea/src/pages/settings/input_page.dart';
import 'package:tarea/src/pages/settings/list_page.dart';
import 'package:tarea/src/pages/settings/tune_page.dart';



void main() => runApp(MyApp());

class MyApp extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      debugShowCheckedModeBanner: false,
      home:  MyBottomNavigation(),
      routes: {
        'alert' : (context) => AlertPage(),
        'avatar' : (context) => AvatarPage(),
        'cart' : (context) => CardsPage(),
        'animated' : (context) => AnimatedPage(),
        'input' : (context) => InputPage(),
        'slider' : (context) => TunePage(),
        'list' : (context) => ListPage(),
      },
    );
  }

}


/*
    return MaterialApp(
      title: 'Material App',
      debugShowCheckedModeBanner: false,
      //home: HomePage(),
      //home: HomePageTemp(),
      initialRoute: 'cart',
      routes: {
        'cart' : (context) => HomePage(),
        'alert' : (context) => AlertPage(),
        'avatar' : (context) => AvatarPage(),
      },

    );

*/